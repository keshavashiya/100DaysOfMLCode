# 100 Days Of ML - LOG

## Day 1 : Jul 10 , 2018

**Today's Progress** : I have practiced implementing Sentimental Analysis with Keras.

**Link of Work:**   [Commit](https://github.com/mhjhamza/100DaysOfMLCode/blob/master/Day1/Day1.ipynb)

## Day 2 : Jul 11 , 2018

**Today's Progress** : Read Research Paper "Learning Spatiotemporal Features with 3D Convolutional Networks"

**Link of Work:**   [Paper](https://goo.gl/j2qixZ)

## Day 3 : Jul 12 , 2018

**Today's Progress** : Learned and implemented Keras Functional API for defining complex models, such as multi-output models, directed acyclic graphs, and models with shared layers.

**Link of Work:**   [commit](https://github.com/mhjhamza/100DaysOfMLCode/blob/master/Code/Day3.ipynb)

## Day 4 : Jul 13 , 2018

**Today's Progress** : Experimented with 'Predict Pakistan Elections 2018' Kaggle kernel by [Ammar Malik](https://www.kaggle.com/ammarmalik)

**Link of Work:**   [commit](https://github.com/mhjhamza/100DaysOfMLCode/blob/master/Code/Day4.ipynb)

## Day 5 : Jul 14 , 2018

**Today's Progress** : Learned and practiced Data Wrangling, Data Visualization with Matplotlib and Seaborn, and basic maths and statistics on a CARS dataset.

**Link of Work:**   [commit](https://github.com/mhjhamza/100DaysOfMLCode/blob/master/Code/Day5.ipynb)

## Day 6 : Jul 15 , 2018

**Today's Progress** : Learned and practiced Dimensionality Reduction techniques and Outlier Analysis on IRIS dataset.

**Link of Work:**   [commit](https://github.com/mhjhamza/100DaysOfMLCode/blob/master/Code/Day6.ipynb)

## Day 7 : Jul 16 , 2018

**Today's Progress** : Learned and practiced K-Means Clustering, K-NN, Linear Regression, Logistic Regression, and Naive Bayes Classifier.

**Link of Work:**   [commit](https://github.com/mhjhamza/100DaysOfMLCode/blob/master/Code/Day7.ipynb)

## Day 8 : Jul 17 , 2018

**Today's Progress** : Learned and practiced Web-based Data visualization with Plotly.

**Link of Work:**   [commit](https://github.com/mhjhamza/100DaysOfMLCode/blob/master/Code/Day8.ipynb)

## Day 9 : Jul 18 , 2018

**Today's Progress** : Learned and practiced BeautifulSoup for parsing HTML webpages. 

**Link of Work:**   [commit](https://github.com/mhjhamza/100DaysOfMLCode/blob/master/Code/Day9.ipynb)

## Day 10 : Jul 19 , 2018

**Today's Progress** : Read articles from the book "HBR Guide to Data Analytics Basics for Managers".

**Link of Work:**   [Book](https://hbr.org/product/hbr-guide-to-data-analytics-basics-for-managers/10185-PBK-ENG)

## Day 11 : Jul 20 , 2018

**Today's Progress** : Took "Become a Data Scientist" course on Lynda.

**Link of Work:**   [Course](https://www.linkedin.com/learning/paths/become-a-data-scientist)

## Day 12 : Jul 21 , 2018

**Today's Progress** : Took "Data Science Foundations: Fundamentals" course on Lynda.

**Link of Work:**   [Course](https://www.linkedin.com/learning/data-science-foundations-fundamentals/exploratory-graphs)

## Day 13 : Jul 22 , 2018

**Today's Progress** : Completed "Data Science Foundations: Fundamentals" course on LinkedIn Learning.

**Link of Work:**   [Certificate](https://goo.gl/iHk4eK)

## Day 14 : Jul 23 , 2018

**Today's Progress** : Completed "Statistics Foundations 1" course on LinkedIn Learning.

**Link of Work:**   [Certificate](https://drive.google.com/open?id=1uR47qy6NmREhD0mYyvwq5zdrxKg7zhLY)

## Day 15 : Jul 24 , 2018

**Today's Progress** : Took "Data Mining" course on LinkedIn Learning and practiced Clustering, Classification and Outlier Detection.

**Link of Work:**   [Course](https://www.linkedin.com/learning/data-science-foundations-data-mining/welcome)

## Day 16 : Jul 25 , 2018

**Today's Progress** : Took "Data Visualization: Storytelling" course on LinkedIn Learning.

**Link of Work:**   [Course](https://www.linkedin.com/learning/data-visualization-storytelling/)

## Day 17 : Jul 26 , 2018

**Today's Progress** : Took "Excel 2016: Managing and Analyzing Data" course on LinkedIn Learning.

**Link of Work:**   [Course](https://www.linkedin.com/learning/excel-2016-managing-and-analyzing-data)
